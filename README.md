# Wardrobify

Team:

* Jessica - Hats
* Jessica - Shoes

## Design

## Shoes microservice

I didn't end up having a partner, so I did the shoes portion as well. I spent a fair amount of time figuring out how to use React Router with the dynamic Routes and linking those to an ID to call the API for the detail. I finally figured that out with the shoes! Got a hacky solution with the Hats.

## Hats microservice

1. Models:
  - Hat Model: main model capturing the details of a hat
  - Location Vo model: model capturing the polling results from Wardrobe

2. Polling:
  - Configured polling to access location data from Wardrobe

3. RESTful API:
  - Set up all API HTTP requests to handle GET, POST, PUT, DELETE

4. Frontend:
  - Configured React components to display:
    - List of hats
    - Select a hat and dynamically change the link to the hat id property
    - Display a hat detail page with the ability to delete


