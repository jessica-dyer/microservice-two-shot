import React from 'react';
import { Link } from 'react-router-dom';

class HatList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hats: [],
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ hats: data.hats })
    }

  }

  render() {
    return (
      <>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
          <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a hat!</Link>
        </div>
        {this.state.hats.map((hat) => (
          <Link
            style={{ display: "block", margin: "1rem 0" }}
            to={`/hats/${hat.id}`}
            key={hat.id}
          >
            {hat.style_name}
          </Link>

        ))}
      </>
    );
  };
}

export default HatList;