import { useParams } from "react-router-dom"
import React from 'react';
import { Link } from 'react-router-dom';



class Hat extends React.Component {
  constructor(props) {
    super(props)
    const myPathName = window.location.pathname
    const parsedId = myPathName.split("/").pop()

    this.state = {
      myId: parsedId,
      hat: '',
      location: '',
      image: '',
    }
    this.handleDelete = this.handleDelete.bind(this);
  }

  async handleDelete(event) {
    event.preventDefault();
    const url = `http://localhost:8090/api/hats/${this.state.myId}/`
    const fetchConfig = {
      method: "delete",
    }
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log("Deleted.")
    }
  }


  async componentDidMount() {

    const url = `http://localhost:8090/api/hats/${this.state.myId}/`
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      this.setState({ hat: data })
      this.setState({ location: data.location })
      this.setState({ image: data.image })
    }
  }

  render() {

    const safeImage = "https://www.ikea.com/us/en/images/products/knorva-hat-blue__0812162_pe771942_s5.jpg?f=xs"

    if (this.state.image === "") {
      this.setState({ image: safeImage })
    }

    return (
      <div className="col">
        <div className="card mb-3 shadow">
          <img src={this.state.image} alt="" className="card-img-top" width="100" />
          <div className="card-body">
            <h5>Hat Details</h5>
            <h6>Color: {this.state.hat.color} </h6>
            <h6>Style: {this.state.hat.style_name} </h6>
            <h6>Fabric: {this.state.hat.fabric} </h6>
            <h6>Location: {this.state.location.closet_name} </h6>
          </div>
        </div>
        <Link to="/hats">
          <button onClick={this.handleDelete} className="btn btn-primary">
            Delete Hat
          </button>
        </Link>
      </div>
    )
  }
}

export default Hat;
