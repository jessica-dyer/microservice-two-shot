import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

function ShoeDetailLink(props) {
  const {
    id,
    manufacturer,
    model_name,
    color
  } = props

  return (
    <Link to={`/shoes/${id}`}
      key={id}>
      {/* <Button variant="primary" size="lg">
        <div>{manufacturer}-{model_name}: {color}</div>
      </Button> */}
      <button type="button" className="btn btn-outline-primary btn-lg btn-block" textDecoration="none">
        <div>{manufacturer}-{model_name}: {color}</div>
      </button>
    </Link>
  )
}

export default ShoeDetailLink

