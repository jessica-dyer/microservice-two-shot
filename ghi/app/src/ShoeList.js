import React, { useEffect, useState, Card } from 'react'
import ShoeDetailLink from './ShoeDetailLink'

const BASE_URL = 'http://localhost:8080/api/shoes/'

export default function ShoeList() {
  const [allShoes, setAllShoes] = useState([])

  useEffect(() => {
    fetch(BASE_URL)
      .then(res => res.json())
      .then(data => {
        setAllShoes(data.shoes)
      })
  }, [])

  return (
    <>
      <div className="d-grid gap-2">
        {allShoes.map(shoe =>
          <ShoeDetailLink
            key={shoe.id}
            manufacturer={shoe.manufacturer}
            id={shoe.id}
            model_name={shoe.model_name}
            color={shoe.color}
          />
        )}
      </div>

    </>
  )
}
