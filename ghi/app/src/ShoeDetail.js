import React from 'react'
import { useParams } from "react-router-dom";
import { useEffect, useState } from 'react'
import { Card, Button } from "react-bootstrap";


const ShoeDetails = () => {
  const { shoeID } = useParams()
  const BASE_URL = 'http://localhost:8080/api/shoes'
  const [shoeManufacturer, setCurrentManufacturer] = useState([])
  const [shoeImageUrl, setCurrentImageUrl] = useState([])
  const [shoeModel, setCurrentShoeModel] = useState([])
  const [shoeColor, setCurrentShoeColor] = useState([])
  const [closet, setCurrentCloset] = useState([])

  useEffect(() => {
    const shoeUrl = `${BASE_URL}/${shoeID}/`
    fetch(shoeUrl)
      .then(res => res.json())
      .then(data => {
        setCurrentManufacturer(data.manufacturer)
        setCurrentShoeModel(data.model_name)
        setCurrentImageUrl(data.image)
        setCurrentShoeColor(data.color)
        setCurrentCloset(data.bin.closet_name)
      })
  }, [shoeID])

  async function handleDeleteShoe(e) {
    const url = `${BASE_URL}/${shoeID}/`
    const fetchConfig = {
      method: "delete",
    }
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log("Deleted.")
    }
  }

  return (
    <div className='mt-20'>
      <Card style={{ width: '24rem' }} classname="mt-2">
        <Card.Header>Featured</Card.Header>
        <Card.Img variant='top' src={shoeImageUrl} />
        <Card.Body className='tb-3'>
          <Card.Title className="d-flex justify-content-between align-items-baseline fw-normal mb-3">
            {shoeManufacturer}
          </Card.Title>
          <Card.Subtitle className="mb-2 text-muted">{shoeModel} - {shoeColor}</Card.Subtitle>
          <Card.Text>These shoes are located in {closet}.</Card.Text>
          <Button variant="primary" onClick={handleDeleteShoe}>Delete</Button>{' '}
        </Card.Body>
      </Card>
    </div >


  )
}

export default ShoeDetails