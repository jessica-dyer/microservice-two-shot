import { BrowserRouter, Routes, Route, withRouter } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatsList';
import HatForm from './HatForm';
import Hat from './hat';
import ShoeList from './ShoeList';
import ShoeDetail from './ShoeDetail';
import 'bootstrap/dist/css/bootstrap.min.css'



function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          {/* <Route path="/" element={<MainPage />} /> */}
          <Route path="hats">
            <Route path='' element={<HatList />} />
            <Route path='new' element={<HatForm />} />
            <Route path=":hatId" element={<Hat />} />
          </Route>
          <Route path="shoes" >
            <Route path='' element={<ShoeList />} />
            <Route
              path=":shoeID"
              element={<ShoeDetail />}
            />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
