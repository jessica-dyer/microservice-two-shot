from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
  import_href = models.CharField(max_length=200, unique=True)
  closet_name = models.CharField(max_length=100)

  def __str__(self) -> str:
      return f"{self.closet_name}"


class Hat(models.Model):
  fabric = models.CharField(max_length=200)
  style_name = models.CharField(max_length=200)
  color = models.CharField(max_length=100)
  image = models.URLField(null=True, blank=True)
  location = models.ForeignKey(
    LocationVO,
    related_name="location",
    on_delete=models.CASCADE,
    null=True,
    blank=True,
  )

  def get_api_url(self):
    return reverse("api_show_hat", kwargs={"pk": self.pk})

  def __str__(self):
        return f"{self.style_name} - {self.color}"

  class Meta:
      ordering = ("style_name", "color", "style_name", "image", "location")


# The Hat resource should track its fabric, its style name,
# its color, a URL for a picture, and the location in the
# wardrobe where it exists.