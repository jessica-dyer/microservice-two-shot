# Generated by Django 4.0.3 on 2022-05-04 23:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0007_remove_locationvo_section_number_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(default='temp href', max_length=200, unique=True),
            preserve_default=False,
        ),
    ]
