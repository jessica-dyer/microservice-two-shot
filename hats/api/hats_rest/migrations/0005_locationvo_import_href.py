# Generated by Django 4.0.3 on 2022-05-04 22:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0004_alter_hat_image_alter_hat_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(default='/api/locations/0/', max_length=200, unique=True),
            preserve_default=False,
        ),
    ]
