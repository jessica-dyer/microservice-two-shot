from django.db import models
from django.urls import reverse


# Create your models here.
class BinVO(models.Model):
  import_href = models.CharField(max_length=200, unique=True)
  closet_name = models.CharField(max_length=100)

  def __str__(self):
    return f"{self.closet_name}"

class Shoe(models.Model):
  manufacturer = models.CharField(max_length=200)
  model_name = models.CharField(max_length=200)
  color = models.CharField(max_length=100)
  image = models.URLField(null=True, blank=True)
  bin = models.ForeignKey(
    BinVO,
    related_name="bin",
    on_delete=models.CASCADE
  )

  def get_api_url(self):
    return reverse("api_show_shoe", kwargs={"pk": self.pk})

  def __str__(self) -> str:
      return f"{self.model_name} - {self.color}"

  class Meta:
      ordering = ("manufacturer", "model_name", "color", "image", "bin")

# The Shoe resource should track its manufacturer,
# its model name, its color, a URL for a picture,
# and the bin in the wardrobe where it exists.