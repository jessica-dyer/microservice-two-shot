from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json



# Create your views here.

from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVOListEncoder(ModelEncoder):
  model = BinVO
  properties = ["closet_name", "import_href"]

class ShoeListEncoder(ModelEncoder):
  model = Shoe
  properties = ['manufacturer', "model_name", "color", "id"]

class ShoeDetailencoder(ModelEncoder):
  model=Shoe
  properties = ["manufacturer", 'model_name', 'color', 'image', 'bin']
  encoders = {'bin': BinVOListEncoder(), }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
  if request.method == "GET":
    shoes = Shoe.objects.all()
    return JsonResponse(
      {"shoes": shoes},
      encoder=ShoeListEncoder,
    )
  else:
    content = json.loads(request.body)

    if "bin" in content:
      try:
        bin_href = content["bin"]
        bin = BinVO.objects.get(import_href=bin_href)
        content["bin"] = bin
      except BinVO.DoesNotExist:
        return JsonResponse(
          {"message": "Invalid bin ID"},
          status=400,
        )
    shoe = Shoe.objects.create(**content)
    return JsonResponse(
      shoe,
      encoder=ShoeDetailencoder,
      safe=False,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
  if request.method == 'GET':
    shoe = Shoe.objects.get(id=pk)
    return JsonResponse(
      shoe,
      encoder=ShoeDetailencoder,
      safe=False,
    )
  elif request.method == "DELETE":
    count, _ = Shoe.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})
  else:
    content = json.loads(request.body)
    try:
      bin = BinVO.objects.get(import_href=content["bin"])
      content["bin"] = bin
    except BinVO.DoesNotExist:
      return JsonResponse(
        {"message": "Invalid bin ID"},
        status=400,
      )
    Shoe.objects.filter(id=pk).update(**content)
    shoe = Shoe.objects.get(id=pk)
    return JsonResponse(
      shoe,
      encoder=ShoeDetailencoder,
      safe=False,
    )